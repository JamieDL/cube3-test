$(document).ready(function() {
  $("#image-slider").owlCarousel({
    items: 1,
    loop: true,
    autoplay: true,
    autoplayTimeout: 4000
  });





  var resizeTimer; // Set resizeTimer to empty so it resets on page load

  function resizeFunction() {
    var $windowWidth = $(window).width();

    if($windowWidth > 970){
      var $sliderMargin = $("#image-slider .col-2").css("margin-right");
    } else {
      var $sliderMargin = $("#image-slider .col-2").css("margin-bottom");
    }

    $('#image-slider .col-3 img:first-of-type').css({
        marginBottom: $sliderMargin
    });
  }

  // On resize, run the function and reset the timeout
  // 250 is the delay in milliseconds. Change as you see fit.
  $(window).resize(function() {
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(resizeFunction, 250);
  });

  resizeFunction();



  $('.contact-form-container input').on('focus', function(){
      $(this).siblings('label').addClass('active');
  });

  $('.contact-form-container input').on('focusout', function(){
      var text_value=$(this).val();

      if(text_value!=''){
          $(this).css({
            borderColor: '#000'
          });
      } else {
          $(this).siblings('label').removeClass('active');
          $(this).css({
            borderColor: '#e5e5e5'
          });
      }

  });


  $('#menu-toggle').on('click', function(){
      $('header').toggleClass('active');
      $('.menu-overlay').toggleClass('active');

      if( $('header').hasClass('active') ) {
        $('.toggle-text').text('Close');
        $('body').css({
          overflowY: 'hidden'
        });
      } else{
        $('.toggle-text').text('Menu');
        $('body').css({
          overflowY: 'visible'
        });
      }
  });



  $(function () {
    // initialize skrollr if the window width is large enough
    if ($(window).width() > 1199) {
      var s = skrollr.init({
        forceHeight: false,
        smoothScrolling: false,
        mobileDeceleration: 0.004
      });
    }

    if(s){
        // disable skrollr if the window is resized below 768px wide
        $(window).on('resize', function () {
          if ($(window).width() <= 1199) {
            s.destroy(); // skrollr.init() returns the singleton created above
          }
        });
    }
  });



});



$(window).on('load', function(){
    $('.loading-overlay').fadeOut('fast', function(){
        $(this).remove();
    });
});
